FROM golang:1.9.2 as builder
WORKDIR /go/src/gitlab.com/msvechla/vaultbeat
COPY ./ /go/src/gitlab.com/msvechla/vaultbeat/
RUN go get .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o vaultbeat .

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/msvechla/vaultbeat/vaultbeat .
COPY vaultbeat.yml .
COPY fields.yml .
COPY ./_meta/ ./_meta
ENTRYPOINT ["./vaultbeat"]
