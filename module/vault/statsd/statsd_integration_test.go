// +build integration

package statsd

import (
	"testing"
	"time"

	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/libbeat/tests/compose"
	mbtest "github.com/elastic/beats/metricbeat/mb/testing"
	"github.com/stretchr/testify/assert"
)

func TestFetch(t *testing.T) {
	t.Log("Starting vault via compose..")
	compose.EnsureUpWithTimeout(t, 120, "vault")
	t.Log("Vault is running...")

	f := mbtest.NewEventFetcher(t, getConfig())
	event, err := f.Fetch()
	if err != nil {
		t.Errorf("Error: %s", err)
	}

	time.Sleep(time.Second * 30)

	t.Logf("%s/%s event: %+v", f.Module().Name(), f.Name(), event)

	event, err = f.Fetch()
	if err != nil {
		t.FailNow()
	}

	t.Logf("%s/%s event: %+v", f.Module().Name(), f.Name(), event)
	// {"hostname":"vault","runtime":{"alloc_bytes":5329736,"free_count":109799,"heap_objects":30358,"malloc_count":140157,"num_goroutines":14,"sys_bytes":11671800,"total_gc_pause_ns":3262269,"total_gc_runs":5}}

	// check event data
	hostname := event["hostname"].(string)
	runtime := event["runtime"].(common.MapStr)

	// hostname
	assert.Equal(t, "vault", hostname)

	// runtime
	assert.True(t, runtime["alloc_bytes"].(float64) > 10, "Check if runtime.alloc_bytes is larger than 0")
	assert.True(t, runtime["free_count"].(float64) > 10, "Check if runtime.alloc_bytes is larger than 0")
	assert.True(t, runtime["heap_objects"].(float64) > 10, "Check if runtime.alloc_bytes is larger than 0")
	assert.True(t, runtime["malloc_count"].(float64) > 10, "Check if runtime.alloc_bytes is larger than 0")

}

func getConfig() map[string]interface{} {
	return map[string]interface{}{
		"module":         "vault",
		"enabled":        true,
		"metricsets":     []string{"statsd"},
		"statsd_address": "my_beat:8125",
		"period":         "5s",
		"logging":        map[string]interface{}{"level": "debug", "selectors": []string{"*"}},
	}
}
