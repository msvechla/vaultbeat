package statsd

import (
	"bytes"
	"fmt"
	"net"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/libbeat/logp"
	"github.com/elastic/beats/metricbeat/mb"
)

// MetricSet type defines all fields of the MetricSet
type MetricSet struct {
	mb.BaseMetricSet
	statsdAddress string
	vaultHostname string
	period        time.Duration
}

// Packet is used to efficiently store the data received via udp
type Packet struct {
	Bucket   string
	Value    float64
	Modifier string
	Sampling float64
}

// Gauge stores an instantaneous measurement of a value
type Gauge struct {
	Name  string
	Value float64
}

var (
	// config data
	msData MetricSet

	// channel setup
	wg          sync.WaitGroup
	stream      = make(chan Packet, 10000)
	pauseStream = make(chan struct{})
	playStream  = make(chan struct{})
	quitStream  = make(chan struct{})

	// metric variables
	counters = make(map[string]float64)
	timers   = make(map[string][]float64)
	gauges   = make([]Gauge, 0)

	// listeners
	listener *net.UDPConn
	address  *net.UDPAddr
)

// init registers the MetricSet with the central registry.
// The New method will be called after the setup of the module and before starting to fetch data
func init() {
	if err := mb.Registry.AddMetricSet("vault", "statsd", New); err != nil {
		panic(err)
	}
}

// New create a new instance of the MetricSet
// Part of new is also setting up the configuration by processing additional
// configuration entries if needed.
func New(base mb.BaseMetricSet) (mb.MetricSet, error) {

	logp.Warn("EXPERIMENTAL: The vault statsd metricset is experimental")

	// initialize the wait group
	wg.Add(1)

	// configuration preparation
	conf := struct {
		StatsdAddress string `config:"statsd_address"`
		VaultHostname string `config:"vault_hostname"`
	}{
		StatsdAddress: "0.0.0.0:8125",
		VaultHostname: "vault",
	}

	if err := base.Module().UnpackConfig(&conf); err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	msData = MetricSet{
		BaseMetricSet: base,
		statsdAddress: conf.StatsdAddress,
		vaultHostname: conf.VaultHostname,
		period:        base.Module().Config().Period,
	}

	// start streaming statsd data
	go streamData()
	// second go routine for aggregating stats
	go aggregateData()

	return &msData, nil
}

// Fetch methods implements the data gathering and data conversion to the right format
// It returns the event which is then forward to the output. In case of an error, a
// descriptive error must be returned.
func (m *MetricSet) Fetch() (common.MapStr, error) {

	// pause stream so current data batch can be released and cleared safely
	pauseStream <- struct{}{}

	// release events
	events := releaseData()

	// start stream again
	playStream <- struct{}{}

	return events, nil
}

// streamData starts listening for statsd address and coordinates packet parsing
func streamData() {
	var err error

	address, _ = net.ResolveUDPAddr("udp", msData.statsdAddress)
	listener, err = net.ListenUDP("udp", address)
	if err != nil {
		fmt.Printf("%s", err.Error())
	}

	defer listener.Close()

	// state machine to pause processing while expired data is released and cleared
	for {
		select {
		case <-pauseStream:
			select {
			case <-playStream:
			case <-quitStream:
				wg.Done()
				return
			}
		case <-quitStream:
			wg.Done()
			return
		default:
			readPackets()
		}
	}
}

// readPackets transforms the packets received via UDP into data objects
func readPackets() {
	message := make([]byte, 512)

	// read message from the stream
	listener.SetReadDeadline(time.Now().Add(time.Second * 5))
	n, _, err := listener.ReadFrom(message)

	if err != nil {
		switch err := err.(type) {
		case net.Error:
			if err.Timeout() {
				// ignore timeouts, this usually means nothing is sending data at the moment
				break
			}
		default:
			fmt.Println(err)
		}
	}

	buf := bytes.NewBuffer(message[0:n])

	// regexp for parsing data
	var sanitizeRegexp = regexp.MustCompile("[^a-zA-Z0-9\\-_\\.:\\|@]")
	// regexp for splitting data into the different groups
	var packetRegexp = regexp.MustCompile("([a-zA-Z0-9_.-]+):([-+]?[0-9]*\\.?[0-9]+)\\|(c|s|g|ms)(\\|@([0-9\\.]+))?")
	s := sanitizeRegexp.ReplaceAllString(buf.String(), "")

	// loop through all stat sets
	for _, item := range packetRegexp.FindAllStringSubmatch(s, -1) {
		value, err := strconv.ParseFloat(item[2], 32)
		if err != nil {
			if item[3] == "ms" {
				value = 0
			} else {
				value = 1
			}
		}

		sampleRate, err := strconv.ParseFloat(item[5], 32)
		if err != nil {
			sampleRate = 1
		}

		// create the packet that will be shipped through the pipeline
		var packet Packet
		// remove the vault hostname prefix
		packet.Bucket = strings.Replace(item[1], fmt.Sprintf("%s.", msData.vaultHostname), "", 1)
		// remove the vault. prefix
		packet.Bucket = strings.Replace(packet.Bucket, "vault.", "", 1)
		// avoid errors with empty strings
		if len(packet.Bucket) == 0 {
			logp.Warn("Unable to format bucket name: %s", strings.Replace(item[1], fmt.Sprintf("%s.", msData.vaultHostname), "", 1))
			continue
		}
		packet.Value = value
		packet.Modifier = item[3]
		packet.Sampling = float64(sampleRate)

		stream <- packet
	}
}

// aggregateData listens for incoming packets on the stream and aggregates all values
func aggregateData() {
	for {
		select {
		case s := <-stream:

			switch s.Modifier {
			case "ms":
				_, ok := timers[s.Bucket]
				if !ok {
					var t []float64
					timers[s.Bucket] = t
				}
				timers[s.Bucket] = append(timers[s.Bucket], s.Value)

			case "c":
				_, ok := counters[s.Bucket]
				if !ok {
					counters[s.Bucket] = 0.0
				}
				counters[s.Bucket] += (float64(s.Value) * (1 / s.Sampling))

			case "g":
				gauges = append(gauges, Gauge{Name: s.Bucket, Value: s.Value})
			}
		}
	}
}

// releaseData generates actual output events of the aggregated data
func releaseData() common.MapStr {
	eventData := common.MapStr{}

	// add generic data
	eventData.Put("hostname", msData.vaultHostname)

	// release Gauges and reset them
	for _, g := range gauges {
		logp.Debug("statsd", "gauge: %v", g)
		eventData.Put(g.Name, g.Value)
	}
	gauges = make([]Gauge, 0)

	// release counters and reset them
	for s, c := range counters {
		value := float64(c) / ((float64(msData.period.Seconds()) * float64(time.Second)) / 1e3)
		eventData.Put(fmt.Sprintf("%s.relative", s), value)
		eventData.Put(fmt.Sprintf("%s.count", s), c)
	}
	counters = make(map[string]float64)

	// release timers and reset them
	for s, t := range timers {
		if len(t) > 0 {
			sort.Float64s(t)
			minVal := t[0]
			maxVal := t[len(t)-1]
			count := len(t)

			sum := 0.0
			for _, v := range t {
				sum += v
			}
			meanVal := sum / float64(count)

			eventData.Put(fmt.Sprintf("%s.min", s), minVal)
			eventData.Put(fmt.Sprintf("%s.max", s), maxVal)
			eventData.Put(fmt.Sprintf("%s.mean", s), meanVal)
		}

	}
	counters = make(map[string]float64)

	return eventData
}
