package sys_health

import (
	"time"

	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/libbeat/logp"
	"github.com/elastic/beats/metricbeat/mb"
	"github.com/hashicorp/vault/api"
)

// init registers the MetricSet with the central registry.
// The New method will be called after the setup of the module and before starting to fetch data
func init() {
	if err := mb.Registry.AddMetricSet("vault", "sys_health", New); err != nil {
		panic(err)
	}
}

// MetricSet type defines all fields of the MetricSet
// As a minimum it must inherit the mb.BaseMetricSet fields, but can be extended with
// additional entries. These variables can be used to persist data or configuration between
// multiple fetch calls.
type MetricSet struct {
	mb.BaseMetricSet
	vaultToken         string
	vaultAddr          string
	vaultCACert        string
	vaultCAPath        string
	vaultClientCert    string
	vaultClientKey     string
	vaultClientTimeout string
	vaultSkipVerify    bool
	vaultTLSServerName string
}

var msData MetricSet
var vaultClient *api.Client

// New create a new instance of the MetricSet
// Part of new is also setting up the configuration by processing additional
// configuration entries if needed.
func New(base mb.BaseMetricSet) (mb.MetricSet, error) {
	logp.Warn("EXPERIMENTAL: The vault sys_health metricset is experimental")

	// configuration preparation
	config := struct {
		VaultToken         string `config:"vault_token"`
		VaultAddr          string `config:"vault_addr"`
		VaultCACert        string `config:"vault_cacert"`
		VaultCAPath        string `config:"vault_capath"`
		VaultClientCert    string `config:"vault_client_cert"`
		VaultClientKey     string `config:"vault_client_key"`
		VaultClientTimeout string `config:"vault_client_timeout"`
		VaultSkipVerify    bool   `config:"vault_skip_verify"`
		VaultTLSServerName string `config:"vault_tls_server_name"`
	}{
		VaultToken:         "",
		VaultAddr:          "http://localhost:8200",
		VaultCACert:        "",
		VaultCAPath:        "",
		VaultClientCert:    "",
		VaultClientKey:     "",
		VaultClientTimeout: "5s",
		VaultSkipVerify:    false,
		VaultTLSServerName: "",
	}

	if err := base.Module().UnpackConfig(&config); err != nil {
		logp.Err(err.Error())
		return nil, err
	}

	logp.Debug("sys_health", "config_raw: %s", config)

	msData = MetricSet{
		BaseMetricSet:      base,
		vaultToken:         config.VaultToken,
		vaultAddr:          config.VaultAddr,
		vaultCACert:        config.VaultCACert,
		vaultCAPath:        config.VaultCAPath,
		vaultClientCert:    config.VaultClientCert,
		vaultClientKey:     config.VaultClientKey,
		vaultClientTimeout: config.VaultClientTimeout,
		vaultSkipVerify:    config.VaultSkipVerify,
		vaultTLSServerName: config.VaultTLSServerName,
	}

	vaultClient = createClient()

	return &msData, nil
}

// Fetch methods implements the data gathering and data conversion to the right format
// It returns the event which is then forward to the output. In case of an error, a
// descriptive error must be returned.
func (m *MetricSet) Fetch() (common.MapStr, error) {
	eventData := common.MapStr{}

	duration, _ := time.ParseDuration(msData.vaultClientTimeout)
	vaultClient.SetClientTimeout(duration)
	healthResponse, err := vaultClient.Sys().Health()

	if err == nil {
		eventData.Put("clusterID", healthResponse.ClusterID)

		eventData.Put("clusterName", healthResponse.ClusterName)

		eventData.Put("initialized.bool", healthResponse.Initialized)
		eventData.Put("initialized.byte", boolToByte(healthResponse.Initialized))

		eventData.Put("sealed.bool", healthResponse.Sealed)
		eventData.Put("sealed.byte", boolToByte(healthResponse.Sealed))

		eventData.Put("serverTimeUTC", healthResponse.ServerTimeUTC)

		eventData.Put("standby.bool", healthResponse.Standby)
		eventData.Put("standby.byte", boolToByte(healthResponse.Standby))

		eventData.Put("version", healthResponse.Version)
	} else {
		logp.Warn("Error fetching sys_health data from vault: %s", err)
	}
	return eventData, err
}

// createClient configures the connection to vault
func createClient() *api.Client {

	timeout, err := time.ParseDuration(msData.vaultClientTimeout)
	if err != nil {
		logp.Err("Unable to parse vault_client_timeout: %s", err)
	}

	config := api.Config{
		Address:    msData.vaultAddr,
		MaxRetries: 0,
		Timeout:    timeout,
	}

	config.ConfigureTLS(&api.TLSConfig{
		CACert:     msData.vaultCACert,
		CAPath:     msData.vaultCAPath,
		ClientCert: msData.vaultClientCert,
		ClientKey:  msData.vaultClientKey,

		TLSServerName: msData.vaultTLSServerName,
		Insecure:      msData.vaultSkipVerify,
	})

	client, err := api.NewClient(&config)
	if err != nil {
		logp.Err("Error initializing client: %s", err.Error())
	}
	logp.Debug("vault_sys_health", "rendered vault config: %s", config.HttpClient)
	logp.Info("Connecting to vault...")

	if msData.vaultToken != "" {
		client.SetToken(msData.vaultToken)

		_, tokenErr := client.Auth().Token().LookupSelf()
		if tokenErr != nil {
			logp.Err("Error connecting to vault: %s", tokenErr)
		}
	}

	return client
}

func boolToByte(boolVar bool) byte {
	if boolVar {
		return byte(1)
	}
	return byte(0)
}
