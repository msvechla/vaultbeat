// +build integration

package sys_health

import (
	"strings"
	"testing"

	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/libbeat/tests/compose"
	mbtest "github.com/elastic/beats/metricbeat/mb/testing"
	"github.com/stretchr/testify/assert"
)

func TestFetch(t *testing.T) {
	t.Log("Starting vault via compose..")
	compose.EnsureUpWithTimeout(t, 120, "vault")
	t.Log("Vault is running...")

	f := mbtest.NewEventFetcher(t, getConfig())
	event, err := f.Fetch()
	if err != nil {
		t.Errorf("Error: %s", err)
	}

	t.Logf("%s/%s event: %+v", f.Module().Name(), f.Name(), event)

	// check event data
	initialized := event["initialized"].(common.MapStr)
	sealed := event["sealed"].(common.MapStr)
	standby := event["standby"].(common.MapStr)
	vaultVersion := event["version"].(string)

	// version
	assert.Equal(t, 3, len(strings.Split(vaultVersion, ".")))

	// booleans
	assert.False(t, initialized["bool"].(bool))
	assert.True(t, sealed["bool"].(bool))
	assert.True(t, standby["bool"].(bool))
	assert.Equal(t, uint8(0x0), initialized["byte"].(byte))
	assert.Equal(t, uint8(0x1), sealed["byte"].(byte))
	assert.Equal(t, uint8(0x1), standby["byte"].(byte))

}

func getConfig() map[string]interface{} {
	return map[string]interface{}{
		"module":     "vault",
		"enabled":    true,
		"metricsets": []string{"sys_health"},
		"vault_addr": "http://vault:8200",
		"logging":    map[string]interface{}{"level": "debug", "selectors": []string{"*"}},
	}
}
