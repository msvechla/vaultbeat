# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [1.0.1] - 2018-03-13
### Fixed
- bumped to latest go version
- removed unnecessary vendor files
- fixed builds for all platforms
## [1.0.0] - 2018-03-13
### Added
- Initial version release
- `sys_health` metricset
- `statsd` metricset
- Bundled Kibana dashboards for quickstart
- Complete field documentation for generating elasticsearch mappings
- Integration tests
- Automated builds for all major platforms
- Docker container published continuously on Dockerhub
- README
- CHANGELOG
- ...
