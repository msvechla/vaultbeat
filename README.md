
[![pipeline status](https://gitlab.com/msvechla/vaultbeat/badges/master/pipeline.svg)](https://gitlab.com/msvechla/vaultbeat/commits/master) [![coverage report](https://gitlab.com/msvechla/vaultbeat/badges/master/coverage.svg)](https://gitlab.com/msvechla/vaultbeat/commits/master)
# vaultbeat

vaultbeat is a beat based on [metricbeat](https://github.com/elastic/beats/tree/master/metricbeat), which collects performance metrics and statistics from [Hashicorp's Vault](https://www.vaultproject.io/).

## Getting started
Gathering metrics from vault is as easy as running one command:
```sh
./vaultbeat -e -c vaultbeat.yml
```
For all available configuration options, see the example [vaultbeat.yml](./vaultbeat.yml) file.

## Running vaultbeat in Docker
An official docker container is provided on [dockerhub](https://hub.docker.com/r/msvechla/vaultbeat/). You can run the container by mounting a custom configuration like this:
```
docker run -d --rm --net=host --name vaultbeat -v /root/vaultbeat.yml:/root/vaultbeat.yml  msvechla/vaultbeat -e -d "*"
```

### Get the latest release

In addition to the [Docker Image](https://hub.docker.com/r/msvechla/vaultbeat/), there are automated builds for all major platforms. You can find all releases in the [tags](https://gitlab.com/msvechla/vaultbeat/tags) section. Simply click on downloads and select "artifacts" to get the desired version. Alternatively feel free to build the latest release from master yourself.

### Available Metricsets
Vaultbeat currently supports two metricsets: `statsd` and `sys_health`.

#### Statsd MetricSet
The statsd metricset utilizes vaults [telemetry endpoint](https://www.vaultproject.io/docs/configuration/telemetry.html#statsd) to gather performance data.

The following configuration options are available:

| Configuratio Option | Description                                                                  |
| ------------------- |------------------------------------------------------------------------------|
| `statsd_address`    | Specifies the address of the statsd endpoint specified in vault.             |
| `vault_hostname`    | Hostname of the vault instance, used for formatting statsd events correctly. | 

#### Sys_health MetricSet
The sys_health metricset connects to vaults `/sys/health` API, to check vaults health status and exports them in an easy to visualize format.

### General Configuration Options
See the [official vault environment variables](https://www.vaultproject.io/docs/commands/index.html#environment-variables) for all available configuration options. Simply specify them in the vaultbeat.yml as usual. Be sure to write them in all lowercase, e.g. `vault_cacert`. For examples see [here](./vaultbeat.yml).


## Bundled Kibana Dashboard
![vaultbeat dashboard](./dashboard_example.png "vaultbeat dashboard")
